<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/EstudianteDAO.php';

class Estudiante
{

    private $codigo;
    private $nombre;
    private $apellido;
    private $fecha_de_nacimiento;
    private $conexion;
    private $estudianteDAO;

    /**
     *
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     *
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     *
     * @return mixed
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     *
     * @return mixed
     */
    public function getFecha_de_nacimiento()
    {
        return $this->fecha_de_nacimiento;
    }

    public function Estudiante($codigo = "", $nombre = "", $apellido = "", $fecha_de_nacimiento = "")
    {
        $this->codigo = $codigo;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->fecha_de_nacimiento = $fecha_de_nacimiento;
        $this->conexion = new Conexion();
        $this->estudianteDAO= new EstudianteDAO($this->codigo,$this->nombre,$this -> apellido,$this->fecha_de_nacimiento);
    }
    
    public function crear_estudiante(){
      $this->conexion->abrir();
      $this->conexion->ejecutar($this->estudianteDAO->crear_estudiante());
      $this->conexion->cerrar();
        
        
    }
    
    public function consultar_estudiantes(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->estudianteDAO->consultar_estudiantes());
        $estudiantes=array();
        while(($resultado=$this->conexion->extraer())!= null) {
            $estudiante= new Estudiante($resultado[0],$resultado[1],$resultado[2],$resultado[3]);
            array_push($estudiantes,$estudiante);
          
        }
        $this->conexion->cerrar();
        return $estudiantes;
        
    }
    
}

