<?php include "presentacion/inicio.php";


if(isset($_POST["crear"])){
    $estudiante = new Estudiante($_POST["codigo"], $_POST["nombre"], $_POST["apellido"], $_POST["fecha_de_nacimiento"]);
    $estudiante->crear_estudiante();
}
?>


<div class="container">
	<div class="row mt-3">
		<div class="col-4"></div>
		<div class="col-4">
			<div class="card">
				<h5 class="card-header">Crear Estudiante</h5>
				<div class="card-body">
					<?php if(isset($_POST["crear"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							Estudiante registrado correctamente
							<button type="button" class="btn-close" data-bs-dismiss="alert"
								aria-label="Close"></button>
					</div>
					<?php } ?>				
					<form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/crearestudiante.php")?>" >
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">código </label>
							<input type="number" class="form-control" name="codigo" required="required">							
						</div>
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">nombre </label>
							<input type="text" class="form-control" name="nombre" required="required">							
						</div>
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">apellido </label>
							<input type="text" class="form-control" name="apellido" required="required">							
						</div>
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">Fecha de Nacimiento</label>
							<input type="date" class="form-control" name="fecha_de_nacimiento" required="required">
						</div>
						
						
						
						<button type="submit" class="btn btn-primary" name="crear">Crear</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>