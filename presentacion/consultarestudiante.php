<?php 
include "presentacion/inicio.php";
$estudiante = new Estudiante();
$estudiantes=$estudiante->consultar_estudiantes();

?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Estudiantes</h5>
				<div class="card-body">
				
				
								<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Código</th>
								<th>Nombre 						 								
								</th>
								<th>Apellido 											 								
								</th>
								<th>Fecha de nacimiento</th>
								
							</tr>
						</thead>
						<tbody>
							<?php
                            foreach ($estudiantes as $estudianteActual) {
                                echo "<tr>";
                                echo "<td>" . $estudianteActual ->getCodigo() . "</td>
                                      <td>" . $estudianteActual -> getNombre() . "</td>
                                      <td>" . $estudianteActual -> getApellido() . "</td>
                                      <td>" . $estudianteActual -> getFecha_de_nacimiento() . "</td>";
                                      
                                echo "</tr>";
                            }
                            ?>
						</tbody>
					</table>
				
				
					
						
						

				</div>
			</div>
		</div>
	</div>
</div>